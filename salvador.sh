#!/bin/bash

# Set shell options
set -e
set -u
set -o 'pipefail'
shopt -s 'dotglob'
shopt -s 'extglob'
shopt -s 'nullglob'

# Set global variables
__version__='3.1.5'
__binary__="$(basename "$0")"

nvchecker_installed="$(command -v nvchecker || true)"
jq_installed="$(command -v jq || true)"
curl_installed="$(command -v curl || true)"

# Helper functions
is_variable_set() { # $1 - name of the variable
	declare -p "$1" &>/dev/null
}

log() { # $1 - text
	printf '\033[1m>>> %s\033[0m\n' "$1"
}

get_directory_for_package() { # $1 - package
	local package_env_file
	package_env_file="$(find "${dir}" -mindepth 2 -maxdepth 2 -type f -name "$1.env")"

	if [[ -n "${package_env_file}" ]]; then
		# Check for an env file with the package name
		local package_env_file_directory
		package_env_file_directory="$(dirname "${package_env_file}")"

		basename "${package_env_file_directory}"
	elif [[ -d "${dir}/$1" ]]; then
		# Check for a directory with the package name
		printf '%s' "$1"
	else
		log "ERROR: Unable to find directory for package $1, aborting..."
		exit 1
	fi
}

prepare_package() { # $1 - package name
	log "Preparing $1"

	local source_files_dir
	local package_dir
	local package_env_file
	source_files_dir="${dir}/$(get_directory_for_package "$1")"
	package_dir="${prepare_dir}/$1"
	package_env_file="$(find "${dir}" -mindepth 2 -maxdepth 2 -type f -name "$1.env")"

	# Create the package directory if it wasn't created by clone_reposit doesn't already exist (=
	if ! [[ -d "${package_dir}/" ]]; then
		mkdir "${package_dir}/"
	fi

	# Copy common files to the package directory if we have any
	if [[ -d "${dir}/_common/" ]]; then
		cp "${dir}/_common/"!(nvchecker.toml|.|..) "${package_dir}/"
	fi

	# Check if we have an env file
	if [[ -n "${package_env_file}" ]]; then
		# Source the env file
		# shellcheck disable=SC1090 # This is provided by the user and has no fixed contents
		source "${package_env_file}"

		# Prepare env var lists
		local env_vars
		local envsubst_vars
		local var
		env_vars="$(sed -r 's/^(.*)=.*/\1/' "${package_env_file}")"
		envsubst_vars="$(echo "${env_vars}" | sed -r 's/^(.*)$/${\1}/')"

		# Export all env vars
		while read -r var; do
			# shellcheck disable=SC2163 # env_vars only contains variable names
			export "${var}"
		done <<< "${env_vars}"

		# Substitute env vars in the template PKGBUILD and output in to the package directory
		envsubst "${envsubst_vars}" < "${source_files_dir}/template/PKGBUILD" > "${package_dir}/PKGBUILD"

		# If there are any files other than PKGBUILD and a nvchecker specification, copy them to the package directory
		if compgen -G "${source_files_dir}/template/!(PKGBUILD|nvchecker.toml|.|..)" > '/dev/null'; then
			cp "${source_files_dir}/template/"!(PKGBUILD|nvchecker.toml|.|..) "${package_dir}/"
		fi

		# Unset all env vars
		while read -r var; do
			unset "${var}"
		done <<< "${env_vars}"
	else
		# If we don't have an env file, just copy all the files except the nvchecker specification
		cp "${source_files_dir}/"!(nvchecker.toml|.|..) "${package_dir}/"
	fi

	# Add a line to the top of the PKGBUILD file with maintainer info
	local name
	local email
	local pkgbuild_content
	name="$(git config --get user.name)"
	email="$(git config --get user.email)"
	pkgbuild_content="$(cat "${package_dir}/PKGBUILD")"

	printf '# Maintainer: %s <%s>\n%s' "${name}" "${email}" "${pkgbuild_content}" > "${package_dir}/PKGBUILD"
}

attempt_to_change_pkgbuild_variable() { # $1 - file path, $2 - variable name, $3 - variable value (or ++/--)
	# Check if the file exists
	if ! [[ -f "$1" ]]; then
		return 1
	fi

	# Make sure the file we're working with has the variable we want to change
	if ! grep -q -E "^(template_)?$2=['\"][^'\"]+['\"].*$" "$1"; then
		return 1
	fi

	# If we're increasing or decreasing the variable, check that it has an numerical value
	if [[ "$3" = '++' ]] || [[ "$3" = '--' ]]; then
		if ! grep -q -E "^(template_)?$2=['\"][0-9]+['\"].*$" "$1"; then
			return 1
		fi
	fi

	# Change the file
	if [[ "$3" = '++' ]]; then
		perl -i -p -e "s/^(template_)?($2=['\"])(\d+)(['\"])/\$1.\$2.(\$3+1).\$4/e" "$1"
	elif [[ "$3" = '--' ]]; then
		perl -i -p -e "s/^(template_)?($2=['\"])(\d+)(['\"])/\$1.\$2.(\$3-1).\$4/e" "$1"
	else
		perl -i -p -e "s/^(template_)?($2=['\"])([^'\"]+)(['\"])/\$1.\$2.'$3'.\$4/e" "$1"
	fi

	# Return a zero return code
	return 0
}

change_pkgbuild_variable() { # $1 - package name, $2 - variable name, $3 - variable value (or ++/--)
	# Check if the package has already been changed, if it was, skip it
	# shellcheck disable=SC2310 # We act based on the return value of is_variable_set
	if is_variable_set 'changed_packages' && printf '%s\n' "${changed_packages[@]}" | grep -q "^$1$"; then
		return 0
	fi

	# Log the variable change
	if [[ "$3" = '++' ]]; then
		log "Increasing the $2 variable of $1"
	elif [[ "$3" = '--' ]]; then
		log "Decreasing the $2 variable of $1"
	else
		log "Changing the $2 variable of $1 to $3"
	fi

	# Check if this package name has an env file
	local package_file
	package_file="$(find "${dir}" -mindepth 2 -maxdepth 2 -type f -name "$1.env")"

	if [[ -n "${package_file}" ]]; then
		# If an env file with the package name exists, try to change it, if that fails, try the template
		# shellcheck disable=SC2310 # We act based on the return value of attempt_to_change_pkgbuild_variable
		if attempt_to_change_pkgbuild_variable "${package_file}" "$2" "$3"; then
			# shellcheck disable=SC2310 # We act based on the return value of is_variable_set
			if is_variable_set 'changed_packages'; then
				changed_packages+=("$1")
			fi

			return 0
		else
			package_file="$(dirname "${package_file}")/template/PKGBUILD"
		fi
	else
		# If no env file was found, try the regular PKGBUILD file
		package_file="${dir}/$1/PKGBUILD"
	fi

	# Try to change our PKGBUILD or template file
	# shellcheck disable=SC2310 # We act based on the return value of attempt_to_change_pkgbuild_variable
	if attempt_to_change_pkgbuild_variable "${package_file}" "$2" "$3"; then
		# shellcheck disable=SC2310 # We act based on the return value of is_variable_set
		if is_variable_set 'changed_packages'; then
			changed_packages+=("$1")
		fi

		return 0
	else
		# We have nothing to change, log that and return a non-zero return code
		log "WARNING: Unable to change the $2 variable of $1"
		return 1
	fi
}

# Common logic
bootstrap_directory_structure() {
	dir="$(pwd)"

	# Make sure we were able to get our PWD
	if [[ -z "${dir}" ]]; then
		log "ERROR: Unable to get the current directory, aborting..."
		exit 1
	fi

	tmp_dir="${dir}/_tmp"
	source_dir="${tmp_dir}/source"
	prepare_dir="${tmp_dir}/prepare"
	build_dir="${tmp_dir}/build"

	if [[ -d "${tmp_dir}/" ]]; then
		rm -rf "${tmp_dir:?}/"
	fi

	local directory

	for directory in "${tmp_dir}" "${source_dir}" "${prepare_dir}" "${build_dir}"; do
		mkdir "${directory}/";
	done
}

log_packages() { # $1 - command name, $@ - packages that changed
	command="$1"
	shift 1

	if [[ "$#" -gt 0 ]]; then
		local text
		text='The following packages have'

		if [[ "${command}" = 'test' ]]; then text="${text} been built"
		elif [[ "${command}" = 'publish' ]]; then text="${text} been published to the AUR"
		elif [[ "${command}" = 'set-variable' ]]; then text="${text} had a variable changed"
		elif [[ "${command}" = 'bump-pkgrels' ]]; then text="${text} had their pkgrel bumped"; fi
		text="${text}:"

		log "${text}"

		local package

		for package in "$@"; do
			echo "${package}"
		done
	fi
}

# Test & publish logic
get_packages() { # $1 - current, last_commit or all
	local directories

	if [[ "$1" = 'in_current' ]]; then
		# Mark files with intent to add and filter out the warning from the output
		local git_add_output
		local print
		local line
		git_add_output="$(git add --intent-to-add -- * 2>&1)"
		print=1

		while read -r line; do
			if [[ "${line}" = 'The following paths are ignored by one of your .gitignore files:' ]]; then
				print=0
			elif [[ "${line}" = 'hint: Disable this message with "git config advice.addIgnoredFile false"' ]]; then
				print=1
			elif [[ "${print}" = '1' ]]; then
				echo "${line}"
			fi
		done <<< "${git_add_output}"

		# Get changed directories in the current state of the repo and filter to only keep root directory names
		directories="$(git diff HEAD --dirstat=files,0 | sed -E 's|^.*% ([^/]*).*$|\1|g')"
	elif [[ "$1" = 'since_last_commit' ]]; then
		# Get directories that changed in the last commit on this branch and filter to only keep root directory names
		directories="$(git diff HEAD~1 HEAD --dirstat=files,0 | sed -E 's|^.*% ([^/]*).*$|\1|g')"
	elif [[ "$1" = 'all' ]]; then
		# Get all files git knows about, exclude the common directory, exclude files not in a directory, get only the directory name, sort and get a list of unique directory names
		directories="$(git ls-files --cached --others --exclude-standard | grep -av '_common/' | grep -a '/' | sed -E 's|([^/]*)/.*|\1|g' | sort | uniq)"
	fi

	# Check if there any directories to work with
	if [[ "${#directories}" -gt 0 ]]; then
		# Sort and deduplicate the directories and convert them to an array
		directories="$(echo "${directories}" | sort | uniq)"
		mapfile -t directories <<< "${directories}"

		# Gather packages for each directory
		local directory

		for directory in "${directories[@]}"; do
			if [[ -d "${directory}/template/" ]]; then
				# If we have a template directory, each package has its env file
				local env

				for env in "${directory}/"*'.env'; do
					basename "${env}" | sed 's/.env$//'
				done
			else
				# If not, just use the directory name
				basename "${directory}"
			fi
		done
	fi
}

prepare_packages() { # $@ - packages
	if [[ "$#" -eq 0 ]]; then
		# If no packages are specified, prepare all of them
		local directory

		for directory in "${dir}/"!(_common)'/'; do
			if ! git check-ignore -q "${directory}"; then
				if [[ -d "${directory}template/" ]]; then
					# If we have a template directory, prepare a package for each env file
					local env

					for env in "${directory}"*'.env'; do
						local package_name
						package_name="$(basename "${env}" | sed 's/.env$//')"

						prepare_package "${package_name}"
					done
				else
					# If not, we're working with a regular package
					local package_name
					package_name="$(basename "${directory}")"

					prepare_package "${package_name}"
				fi
			fi
		done
	else
		# If packages are specified, prepare each of them
		local package

		for package in "$@"; do
			prepare_package "${package}"
		done
	fi
}

get_sources() {
	local package

	# Go through each package
	for package in "${prepare_dir}/"*'/'; do
		cd "${package}" || { log "ERROR: unable to move to directory ${package}, aborting..."; exit 1; }

		local package_name
		local package_source_dir
		package_name="$(basename "${package}")"
		package_source_dir="${source_dir}/${package_name}"
		log "Getting sources for ${package_name}"

		# Run updpkgsums, which downloads all sources and updates checksums
		if ! SRCDEST="${package_source_dir}" updpkgsums "${package}PKGBUILD"; then
			log "ERROR: Getting sources for ${package_name} failed, aborting..."
			exit 2
		fi

		# Regenerate the .SRCINFO file
		makepkg --printsrcinfo | sed -z '$ s/\n\n$/\n/' > "${package}.SRCINFO"
 	done

	# Be nice and move back to the directory we started in
	cd "${dir}/" || { log "ERROR: unable to move to directory ${dir}/, aborting..."; exit 1; }
}

# Test logic
build_packages() {
	local package

	# Go through each package
	for package in "${prepare_dir}/"*'/'; do
		cd "${package}" || { log "ERROR: unable to move to directory ${package}, aborting..."; exit 1; }

		local package_name
		package_name="$(basename "${package}")"
		log "Building ${package_name}"

		# Build the package and check if the package was built correctly
		if SRCDEST="${source_dir}/${package_name}" BUILDDIR="${build_dir}" PKGDEST="${build_dir}/${package_name}" makepkg -s; then
			# If we're collecting changed packages, add the built package
			# shellcheck disable=SC2310 # We act based on the return value of is_variable_set
			if is_variable_set 'changed_packages'; then
				changed_packages+=("${package_name}")
			fi
		else
			log "WARNING: Unable to build ${package_name}"
		fi
 	done

	# Be nice and move back to the directory we started in
	cd "${dir}/" || { log "ERROR: unable to move to directory ${dir}/, aborting..."; exit 1; }
}

# Publish logic
clone_repos() { # $@ - packages
	local package

	# Go through each package
	for package in "$@"; do
		local package_dir
		package_dir="${prepare_dir}/${package}"

		# Clone the repo from the AUR
		if ! git clone --quiet -c 'init.defaultBranch=master' "ssh://aur@aur.archlinux.org/${package}.git" "${package_dir}/"; then
			log "ERROR: Getting repo for ${package} from AUR failed, aborting..."
			exit 2
		fi

		# Delete any existing files
		rm -rf "${package_dir:?}/"!(.git|.|..)
 	done
}

publish_changes() {
	local package

	# Go through each package
	for package in "${prepare_dir}/"*'/'; do
		cd "${package}" || { log "ERROR: unable to move to directory ${package}, aborting..."; exit 1; }

		# Add all changes
		git add -A

		# Check if any changes actually happened
		local git_status_output
		git_status_output="$(git status --porcelain=v1 2> '/dev/null')"

		if [[ -n "${git_status_output}" ]]; then
			local package_name
			package_name="$(basename "${package}")"
			log "Publishing ${package_name}"

			# Commit and push the changes
			local git_message
			git_message="$(git -C "${dir}" log --pretty=%B -n 1)"

			git commit -m "${git_message}"

			if ! git push; then
				log "ERROR: Publishing ${package_name} failed, aborting..."
				exit 2
			fi

			# If we're collecting changed packages, add the published package
			# shellcheck disable=SC2310 # We act based on the return value of is_variable_set
			if is_variable_set 'changed_packages'; then
				changed_packages+=("${package_name}")
			fi
		fi
 	done

	# Be nice and move back to the directory we started in
	cd "${dir}/" || { log "ERROR: unable to move to directory ${dir}/, aborting..."; exit 1; }
}

# bump-pkgrels logic
bump_package_pkgrels() { # $@ - packages whose dependents' pkgrel should be bumped
	local package

	# For each of our packages, go through all specified packages
	for package in "${prepare_dir}/"*'/'; do
		cd "${package}" || { log "ERROR: unable to move to directory ${package}, aborting..."; exit 1; }
		local package_name
		local dependency_package
		local srcinfo
		package_name="$(basename "${package}")"

		for dependency_package in "$@"; do
			# Check if our package depends on this specified package - do a basic grep first, as it's much faster and allows us to filter out files that definitely won't match
			if ! grep -q "${dependency_package}" "${package}PKGBUILD"; then
				# We definitely don't depend on this package, skip it
				continue
			fi

			# Generate a SRCINFO file
			srcinfo="$(makepkg --printsrcinfo)"

			# Check if we depend on this package using the SRCINFO file
			if grep -q -E "^[[:space:]]*(make|check|opt)?depends = ${dependency_package}([>=<].*)?(:.*)?$" <<< "${srcinfo}"; then
				# We have a match, bump the package
				# shellcheck disable=SC2310 # We act based on the return value of change_pkgbuild_variable
				if change_pkgbuild_variable "${package_name}" 'pkgrel' '++'; then
					# We don't want to bump a package more than once, so don't check the rest of the specified packages
					break
				fi
			fi
		done
	done

	# Be nice and move back to the directory we started in
	cd "${dir}/" || { log "ERROR: unable to move to directory ${dir}/, aborting..."; exit 1; }
}

# nvchecker logic
generate_nvchecker_config() {
	find "${dir}" -mindepth 2 -type f -name 'nvchecker.toml' -exec cat {} + > "${dir}/nvchecker.toml"
}

run_nvchecker() {
	# Prepare a keyfile if we have keyfile data
	if [[ -n "${SALVADOR_NVCHECKER_KEYFILE:-}" ]]; then
		printf '%s' "${SALVADOR_NVCHECKER_KEYFILE}" | base64 -d > "${dir}/keyfile.toml"
	fi

	# Run nvchecker itself
	local updates
	updates="$(nvchecker -c "${dir}/nvchecker.toml" --logger=json | jq -c 'select((.event == "updated" and .old_version != null) or .event == "no-result")')"

	# Remove a keyfile if we had keyfile data
	if [[ -n "${SALVADOR_NVCHECKER_KEYFILE:-}" ]]; then
		rm -f "${dir}/keyfile.toml"
	fi

	# Replace the state file with the new one
	rm -f "${dir}/nvchecker.json"
	mv "${dir}/nvchecker.new.json" "${dir}/nvchecker.json"

	# Add all changes
	cd "${dir}/" || { log "ERROR: unable to move to directory ${dir}/, aborting..."; exit 1; }
	git add "${dir}/nvchecker.json"

	# Check if the nvchecker state changed
	local git_status_output
	git_status_output="$(git status --porcelain=v1 "${dir}/nvchecker.json" 2> '/dev/null')"

	if [[ -n "${git_status_output}" ]]; then
		# Commit and push the changes
		log 'Updating nvchecker state'
		git commit -m '[cron] Update nvchecker state'

		if ! git push; then
			log 'ERROR: Updating nvchecker state failed, aborting...'
			exit 2
		fi
	fi

	# Check if there were any updates
	if [[ "${#updates}" -gt 0 ]]; then
		# Convert them to an array
		mapfile -t updates <<< "${updates}"

		# For each update, call an appropriate command
		local update
		local run_command
		local run_command_failed
		run_command_failed='0'

		for update in "${updates[@]}"; do
			local event
			local name
			event="$(printf '%s' "${update}" | jq -r '.event')"
			name="$(printf '%s' "${update}" | jq -r '.name')"

			if [[ "${event}" = 'updated' ]]; then
				local version
				local url
				version="$(printf '%s' "${update}" | jq -r '.version')"
				url="$(printf '%s' "${update}" | jq -r '.url')"

				# Get the directory of the package
				local package_directory
				package_directory="${dir}/$(get_directory_for_package "${name}")"

				# Check if we want to replace the pkgver
				if ! grep -q '_replace\s*=\s*false' "${package_directory}/nvchecker.toml"; then
					# Take note of the orginal branch we started at
					local original_branch
					original_branch="$(git rev-parse --abbrev-ref HEAD)"

					# Figure out the name of the new branch
					local new_branch
					new_branch="update/${name}"

					# Fetch any new remote branches
					git fetch origin

					# Switch to the new branch (creating it if it doesn't exist)
					log "Preparing update for ${name}"
					git switch "${new_branch}" 2> '/dev/null' || git switch --create "${new_branch}"

					# Change the pkgver and pkgrel variables
					# shellcheck disable=SC2310 # We act based on the return value of change_pkgbuild_variable
					if ! change_pkgbuild_variable "${name}" 'pkgver' "${version}"; then
						# Switch back to the original branch and abort
						git switch "${original_branch}"
						exit 1
					fi

					# shellcheck disable=SC2310 # We act based on the return value of change_pkgbuild_variable
					if ! change_pkgbuild_variable "${name}" 'pkgrel' '1'; then
						# Switch back to the original branch and abort
						git switch "${original_branch}"
						exit 1
					fi

					# Add all changes
					git add -A

					# Check if any changes actually happened
					local git_update_status_output
					git_update_status_output="$(git status --porcelain=v1 2> '/dev/null')"

					if [[ -n "${git_update_status_output}" ]]; then
						# Commit and push the changes
						log "Pushing update for ${name}"
						git commit -m "[${name}] Update to ${version}"

						if ! git push --set-upstream origin "${new_branch}"; then
							# Switch back to the original branch
							git switch "${original_branch}"

							# Log an error and abort
							log 'ERROR: Pushing an update branch failed, aborting...'
							exit 2
						fi
					else
						log "No changes resulted from ${name} update"
					fi

					# Switch back to the original branch
					git switch "${original_branch}"

					# Figure out a command for creating a MR
					run_command="${SALVADOR_CUSTOM_COMMAND:-${SALVADOR_MR_COMMAND:-$(dirname -- "$0")/../lib/${__binary__}/create-gitlab-mr}}"
				else
					# Figure out a command for creating an issue
					run_command="${SALVADOR_CUSTOM_COMMAND:-${SALVADOR_ISSUE_COMMAND:-$(dirname -- "$0")/../lib/${__binary__}/create-gitlab-issue}}"
				fi

				# Run the command with the update information
				run_command="$(realpath -m "${run_command}")"

				if ! [[ -f "${run_command}" ]]; then
					log "ERROR: Unable to find command ${run_command}, aborting..."
					run_command_failed='1'
					continue
				fi

				if [[ "${url}" != 'null' ]]; then
					log "Calling command ${run_command} for version ${version} of package ${name} with URL ${url}"
					if ! "${run_command}" "${name}" "${version}" "${url}"; then
						run_command_failed='1'
					fi
				else
					log "Calling command ${run_command} for version ${version} of package ${name}"
					if ! "${run_command}" "${name}" "${version}"; then
						run_command_failed='1'
					fi
				fi
			else
				# Figure out what command we'll be using for unavailable packages
				run_command="${SALVADOR_CUSTOM_COMMAND:-${SALVADOR_ISSUE_COMMAND:-$(dirname -- "$0")/../lib/${__binary__}/create-gitlab-issue}}"

				# Run the command with the unavailable package information
				run_command="$(realpath -m "${run_command}")"

				if ! [[ -f "${run_command}" ]]; then
					log "ERROR: Unable to find command ${run_command}, aborting..."
					run_command_failed='1'
					continue
				fi

				log "Calling command ${run_command} for package ${name} with no version detected"
				if ! "${run_command}" "${name}"; then
					run_command_failed='1'
				fi
			fi
		done

		# If we have a flag marking that at least one command run failed, exit with a non-zero exit code to make sure the pipeline gets marked as failed
		if [[ "${run_command_failed}" = '1' ]]; then
			log 'ERROR: At least one command run failed, exiting with a non-zero exit code...'
			exit 1
		fi
	fi
}

# Command handlers
test_handler() { # $@ - packages
	# Prepare the directory structure
	bootstrap_directory_structure

	# Figure out which packages we'll be working with
	local packages

	if [[ "$#" -eq 0 ]] || [[ "$1" = 'all' ]]; then
		if [[ "$#" -eq 0 ]]; then
			# Get packages that changed
			packages="$(get_packages 'in_current')"
		else
			# Get all packages
			packages="$(get_packages 'all')"
		fi

		# Sort them and convert them to an array
		packages="$(echo "${packages}" | sort)"
		mapfile -t packages <<< "${packages}"
	else
		# We have specified packages, use them
		packages=("$@")
	fi

	# Check if we have any packages to work with
	if [[ "${#packages}" -eq 0 ]]; then
		log 'No packages to test'
		exit 0
	fi

	# Prepare packages
	prepare_packages "${packages[@]}"

	# Get sources for the prepared packages
	get_sources

	# Prepare an empty array for collecting built packages
	changed_packages=()

	# Build the prepared packages
	build_packages

	# Log packages that have been built
	log_packages 'test' "${changed_packages[@]}"
}

publish_handler() { # $@ - packages
	# Prepare the directory structure
	bootstrap_directory_structure

	# Figure out which packages we'll be working with
	local packages

	if [[ "$#" -eq 0 ]] || [[ "$1" = 'all' ]]; then
		if [[ "$#" -eq 0 ]]; then
			# Get packages that changed
			packages="$(get_packages 'since_last_commit')"
		else
			# Get all packages
			packages="$(get_packages 'all')"
		fi

		# Sort them and convert them to an array
		packages="$(echo "${packages}" | sort)"
		mapfile -t packages <<< "${packages}"
	else
		# We have specified packages, use them
		packages=("$@")
	fi

	# Check if we have any packages to work with
	if [[ "${#packages}" -eq 0 ]]; then
		log 'No packages to publish'
		exit 0
	fi

	# Clone repos from the AUR
	clone_repos "${packages[@]}"

	# Prepare packages
	prepare_packages "${packages[@]}"

	# Get sources for the prepared packages
	get_sources

	# Prepare an empty array for collecting published packages
	changed_packages=()

	# Publish any changes in the packages to the AUR
	publish_changes

	# Log packages that have been published
	log_packages 'publish' "${changed_packages[@]}"
}

set_variable_handler() { # $1 - package, $2 - variable name, $3 - variable value
	# Prepare the directory structure
	bootstrap_directory_structure

	# Prepare an empty array for collecting changed packages
	changed_packages=()

	# Change packages that need to be changed
	# shellcheck disable=SC2310 # We ignore the return value of change_pkgbuild_variable
	change_pkgbuild_variable "$1" "$2" "$3" || true

	# Log packages that have had their variable changed
	log_packages 'set-variable' "${changed_packages[@]}"
}

bump_pkgrels_handler() { # $@ - packages
	# Prepare the directory structure
	bootstrap_directory_structure

	# Prepare all packages
	prepare_packages

	# Prepare an empty array for collecting bumped packages
	changed_packages=()

	# Bump packages that need to be bumped
	bump_package_pkgrels "$@"

	# Log packages that have had their pkgrel bumped
	log_packages 'bump-pkgrels' "${changed_packages[@]}"
}

nvchecker_handler() {
	# Prepare the directory structure
	bootstrap_directory_structure

	# Generate a nvchecker config file
	generate_nvchecker_config

	# Run nvchecker
	run_nvchecker
}

list_all_packages_handler() {
	get_packages 'all'
}

help_handler() { # $1 - exit code
	echo "USAGE
    ${__binary__} COMMAND [PACKAGE]...

COMMANDS
    test [PACKAGE]...
        Build packages changed since the last commit (if PACKAGEs are specified,
        only those are built, if first PACKAGE is 'all', all packages are
        built).

    publish [PACKAGE]...
        Publish packages changed in the last commit (if PACKAGEs are specified,
        only those are published, if first PACKAGE is 'all', all packages are
        published). All packages described in the previous sentence are only
        published if the content of their AUR repo changes.

    set-variable PACKAGE VARIABLE VALUE
        Set the VARIABLE of PACKAGE to VALUE. If VALUE is ++ or --, the variable
        is increased or decreased respectively, given that it's numerical.

    bump-pkgrels PACKAGE...
        Bump the pkgrel of all packages that depend on PACKAGEs."

	if [[ -n "${nvchecker_installed}" ]] && [[ -n "${jq_installed}" ]] && [[ -n "${curl_installed}" ]]; then
		echo "
    nvchecker
        Combine the nvchecker definitions for each package, check for updates,
        commit & push the updated nvchecker state file and create issues/MRs
        accordingly. oldver should be set to 'nvchecker.json', newver
        to 'nvchecker.new.json' and keyfile to 'keyfile.toml' in the global
        nvchecker config. See ENVIRONMENT VARIABLES for configuration details."
	fi

	echo "
    list-all-packages
        Show all available packages and exit. Primarily intended for use by
        a shell tab completion function.

    help
        Show this help message and exit.

    version
        Show the version number and exit."

	if [[ -n "${nvchecker_installed}" ]] && [[ -n "${jq_installed}" ]] && [[ -n "${curl_installed}" ]]; then
		echo "
ENVIRONMENT VARIABLES
    SALVADOR_MR_COMMAND
        The command that is run when salvador decides a merge request should
        be created for an update detected by nvchecker. Called with the package
        name as the first argument, the new package version as the second
        argument and a release URL as the third argument (if available). When
        not set, the included create-gitlab-mr script is used.

    SALVADOR_ISSUE_COMMAND
        The command that is run when salvador decides an issue should be created
        for an update or an unavailable package detected by nvchecker. Called
        with the package name as the first argument, the new package version
        (if available) as the second argument and a release URL as the third
        argument (if available). When not set, the included create-gitlab-issue
        script is used.

    SALVADOR_CUSTOM_COMMAND
        The command that is run when an update or an unavailable package
        is detected by nvchecker. Called with the package name as the first
        argument, the new package version (if available) as the second argument
        and a release URL as the third argument (if available). When set,
        the separate issue and MR commands are ignored, and this command
        is always called.

    SALVADOR_NVCHECKER_KEYFILE
        The contents of a nvchecker keyfile, base64 encoded. If not set,
        no keyfile is created.

    GITLAB_API_TOKEN
        A GitLab API token used by the create-gitlab-mr and create-gitlab-issue
        scripts. Must have the api scope to be able to create MRs and issues.

    GITLAB_ASSIGNEE_ID
        A GitLab user ID used by the create-gitlab-mr and create-gitlab-issue
        to assign MRs and issues. Optional, not assigned if not provided.

    CI_PROJECT_ID
        A GitLab project ID used by the create-gitlab-mr and create-gitlab-issue
        scripts. Usually provided automatically by GitLab CI."
	fi

	if [[ -n "$1" ]]; then
		exit "$1"
	fi
}

version_handler() {
	echo "${__version__}"
	exit 0
}

# Run the appropriate handler
command="${1:-}"
if [[ -n "${command}" ]]; then
	shift 1
fi

if [[ "${command}" = 'test' ]]; then
	test_handler "$@"
elif [[ "${command}" = 'publish' ]]; then
	publish_handler "$@"
elif [[ "${command}" = 'set-variable' ]] && [[ "$#" -eq 3 ]]; then
	set_variable_handler "$@"
elif [[ "${command}" = 'bump-pkgrels' ]] && [[ "$#" -gt 0 ]]; then
	bump_pkgrels_handler "$@"
elif [[ "${command}" = 'nvchecker' ]] && [[ -n "${nvchecker_installed}" ]] && [[ -n "${jq_installed}" ]] && [[ -n "${curl_installed}" ]]; then
	nvchecker_handler
elif [[ "${command}" = 'list-all-packages' ]]; then
	list_all_packages_handler
elif [[ "${command}" = 'help' ]]; then
	help_handler 0
elif [[ "${command}" = 'version' ]]; then
	version_handler
else
	help_handler 1
fi
