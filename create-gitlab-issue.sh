#!/bin/bash

# Set shell options
set -e
set -u
set -o 'pipefail'
shopt -s 'dotglob'
shopt -s 'extglob'
shopt -s 'nullglob'

# Check if we have the name parameter and optionally the version string parameter and the URL parameter
if [[ "$#" -lt 1 ]] || [[ "$#" -gt 3 ]]; then
	echo 'ERROR: The package name and optionally a version string and an URL must be provided as the first, second and third arguments, respectively, aborting...'
	exit 1
fi

# Check if the provided parameters are not empty
if [[ -z "$1" ]]; then
	echo 'ERROR: The package name cannot be an empty string, aborting...'
	exit 1
fi

if [[ "$#" -ge 2 ]] && [[ -z "$2" ]]; then
	echo 'ERROR: The version string cannot be an empty string, aborting...'
	exit 1
fi

if [[ "$#" -ge 3 ]] && [[ -z "$3" ]]; then
	echo 'ERROR: The URL cannot be an empty string, aborting...'
	exit 1
fi

# Check if we have the env vars we need
if [[ -z "${GITLAB_API_TOKEN:-}" ]] || [[ -z "${CI_PROJECT_ID:-}" ]]; then
	echo 'ERROR: Environment variables GITLAB_API_TOKEN and CI_PROJECT_ID are required, aborting...'
	exit 1
fi

# Check if we already have an issue for this package
issue_query="[$1]"
issue_search_temp_file="$(mktemp)"
issue_search_response_status_code="$(curl --silent --output "${issue_search_temp_file}" --write-out '%{response_code}' --request 'GET' --header 'Content-Type: application/json' --header "Private-Token: ${GITLAB_API_TOKEN}" --get --data-urlencode "search=${issue_query}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/issues?in=title&state=opened&order_by=created_at&sort=asc&per_page=100" || true)"
issue_search_response="$(cat "${issue_search_temp_file}")"
rm --force "${issue_search_temp_file}"

if [[ "${issue_search_response_status_code}" -lt 200 ]] || [[ "${issue_search_response_status_code}" -gt 299 ]]; then
	echo 'ERROR: Request to find a unique issue for this package name failed, aborting...'
	printf 'HTTP status code: %s\n' "${issue_search_response_status_code}"
	printf 'HTTP response body: %s\n' "${issue_search_response}"
	exit 2
fi

issue_search_response="$(echo "${issue_search_response}" | jq --arg issue_query "${issue_query}" -r '.[] | select(.title | contains($issue_query))')"
issue_search_count="$(echo "${issue_search_response}" | jq -rs '. | length')"

if [[ "${issue_search_count}" = '1' ]]; then
	issue_id="$(echo "${issue_search_response}" | jq -rs '.[0].iid // ""')"
elif [[ "${issue_search_count}" = '0' ]]; then
	issue_id=''
else
	echo 'ERROR: Unable to find a unique issue for this package name, aborting...'
	exit 2
fi

# Check if we have a version string and an URL and prepare an issue title and description based on that
if [[ "$#" -ge 2 ]]; then
	issue_title="${issue_query} Updated to $2"

	if [[ "$#" -ge 3 ]]; then
		description="Release URL: $3
---
"
	else
		description=''
	fi
else
	issue_title="${issue_query} No version detected"
	description=''
fi

description="${description}Issue created automatically by [salvador](https://gitlab.com/dpeukert/salvador)."

# Check if we want to set an assignee
assignee=''
if [[ -n "${GITLAB_ASSIGNEE_ID:-}" ]]; then
	assignee="assignee_id=${GITLAB_ASSIGNEE_ID}"
fi

# Create or update the issue
url="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/issues"

if [[ -z "${issue_id}" ]]; then
	method='POST'
else
	method='PUT'
	url+="/${issue_id}"
fi

issue_create_update_temp_file="$(mktemp)"
issue_create_update_response_status_code="$(curl --silent --output "${issue_create_update_temp_file}" --write-out '%{response_code}' --request "${method}" --header 'Content-Type: application/json' --header "Private-Token: ${GITLAB_API_TOKEN}" --get --data-urlencode "title=${issue_title}" --data-urlencode "description=${description}" --data-urlencode "${assignee}" "${url}" || true)"
issue_create_update_response="$(cat "${issue_create_update_temp_file}")"
rm --force "${issue_create_update_temp_file}"

if [[ "${issue_create_update_response_status_code}" -lt 200 ]] || [[ "${issue_create_update_response_status_code}" -gt 299 ]]; then
	echo 'ERROR: Request to create/update an issue for this package name failed, aborting...'
	printf 'HTTP status code: %s\n' "${issue_create_update_response_status_code}"
	printf 'HTTP response body: %s\n' "${issue_create_update_response}"
	exit 2
fi
