PREFIX ?= /usr

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p salvador.sh $(DESTDIR)$(PREFIX)/bin/salvador
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/salvador
	@mkdir -p $(DESTDIR)$(PREFIX)/lib/salvador
	@cp -p create-gitlab-issue.sh $(DESTDIR)$(PREFIX)/lib/salvador/create-gitlab-issue
	@chmod 755 $(DESTDIR)$(PREFIX)/lib/salvador/create-gitlab-issue
	@cp -p create-gitlab-mr.sh $(DESTDIR)$(PREFIX)/lib/salvador/create-gitlab-mr
	@chmod 755 $(DESTDIR)$(PREFIX)/lib/salvador/create-gitlab-mr
	@mkdir -p $(DESTDIR)$(PREFIX)/share/bash-completion/completions
	@cp -p bash-completion $(DESTDIR)$(PREFIX)/share/bash-completion/completions/salvador
	@chmod 644 $(DESTDIR)$(PREFIX)/share/bash-completion/completions/salvador

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/salvador
	@rm -rf $(DESTDIR)$(PREFIX)/lib/salvador/
	@rm -rf $(DESTDIR)$(PREFIX)/share/bash-completion/completions/salvador
