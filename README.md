# salvador

`salvador` is a bash script that will help you maintain your AUR packages.

## How do I use this?
```
USAGE
    salvador COMMAND [PACKAGE]...

COMMANDS
    test [PACKAGE]...
        Build packages changed since the last commit (if PACKAGEs are specified,
        only those are built, if first PACKAGE is 'all', all packages are
        built).

    publish [PACKAGE]...
        Publish packages changed in the last commit (if PACKAGEs are specified,
        only those are published, if first PACKAGE is 'all', all packages are
        published). All packages described in the previous sentence are only
        published if the content of their AUR repo changes.

    set-variable PACKAGE VARIABLE VALUE
        Set the VARIABLE of PACKAGE to VALUE. If VALUE is ++ or --, the variable
        is increased or decreased respectively, given that it's numerical.

    bump-pkgrels PACKAGE...
        Bump the pkgrel of all packages that depend on PACKAGEs.

    nvchecker
        Combine the nvchecker definitions for each package, check for updates,
        commit & push the updated nvchecker state file and create issues/MRs
        accordingly. oldver should be set to 'nvchecker.json', newver
        to 'nvchecker.new.json' and keyfile to 'keyfile.toml' in the global
        nvchecker config. See ENVIRONMENT VARIABLES for configuration details.

    list-all-packages
        Show all available packages and exit. Primarily intended for use by
        a shell tab completion function.

    help
        Show this help message and exit.

    version
        Show the version number and exit.

ENVIRONMENT VARIABLES
    SALVADOR_MR_COMMAND
        The command that is run when salvador decides a merge request should
        be created for an update detected by nvchecker. Called with the package
        name as the first argument, the new package version as the second
        argument and a release URL as the third argument (if available). When
        not set, the included create-gitlab-mr script is used.

    SALVADOR_ISSUE_COMMAND
        The command that is run when salvador decides an issue should be created
        for an update or an unavailable package detected by nvchecker. Called
        with the package name as the first argument, the new package version
        (if available) as the second argument and a release URL as the third
        argument (if available). When not set, the included create-gitlab-issue
        script is used.

    SALVADOR_CUSTOM_COMMAND
        The command that is run when an update or an unavailable package
        is detected by nvchecker. Called with the package name as the first
        argument, the new package version (if available) as the second argument
        and a release URL as the third argument (if available). When set,
        the separate issue and MR commands are ignored, and this command
        is always called.

    SALVADOR_NVCHECKER_KEYFILE
        The contents of a nvchecker keyfile, base64 encoded. If not set,
        no keyfile is created.

    GITLAB_API_TOKEN
        A GitLab API token used by the create-gitlab-mr and create-gitlab-issue
        scripts. Must have the api scope to be able to create MRs and issues.

    GITLAB_ASSIGNEE_ID
        A GitLab user ID used by the create-gitlab-mr and create-gitlab-issue
        to assign MRs and issues. Optional, not assigned if not provided.

    CI_PROJECT_ID
        A GitLab project ID used by the create-gitlab-mr and create-gitlab-issue
        scripts. Usually provided automatically by GitLab CI.
```

For an example of the folder structure, check out [my PKGBUILDs repo](https://gitlab.com/dpeukert/pkgbuilds/), but the basic gist of it is this:
- place the files (e.g. `.gitignore`) that you want included in every package in `_common/` - [example](https://gitlab.com/dpeukert/pkgbuilds/-/tree/main/_common/)
- create a folder with the name that you want to use for the package containing the PKGBUILD file and other files if required - [example](https://gitlab.com/dpeukert/pkgbuilds/-/tree/main/weatherspect/)
	- checksums in PKGBUILD files get filled in automatically by `updpkgsums`
- if you want to use a single PKGBUILD file as a template for multiple packages, place it in `whatever/template/` and create `package-name.env` files with the variables you want to replace in the PKGBUILD file in `whatever/` - [example](https://gitlab.com/dpeukert/pkgbuilds/-/tree/main/mongodb-compass/)

If you want to use the nvchecker functionality (make sure you install the optional dependencies, see the next section), add your `__config__` section (including the section name) to `_common/nvchecker.toml` and create a `nvchecker.toml` file for each package you want to track (again, including the section name). If setup correctly, the `nvchecker` command takes care of combinging the TOML files, checking for updates, commiting the changes and running commands when an update is detected (creating GitLab issues and merge requests is the default behaviour). For more details, see the output of the `help` command. This functionality is intended for running via a CI tool such as GitLab CI (see the [Gitlab CI config in my PKGBUILDs repo](https://gitlab.com/dpeukert/pkgbuilds/-/blob/main/.gitlab-ci.yml)), but should also work when running locally.

## Cool, how do I go about installing this thing?

- use the [salvador AUR package](https://aur.archlinux.org/packages/salvador/)
- download this repo and run `make install` (and `make uninstall` when you inevitably decide to write your own script)
  - make sure you have the following dependencies installed
    - `bash`
	- `coreutils`
	- `findutils`
    - `git`
    - `grep`
    - `pacman`
    - `pacman-contrib`
    - `perl`
    - `sed`
    - optional dependencies for the nvchecker functionality
      - `curl`
      - `jq`
      - `nvchecker`

## Why the name?

`salvador` is named after [Salvador Allende](https://en.wikipedia.org/wiki/Salvador_Allende), a Chilean socialist democratically elected as President of Chile in 1970 and overthrown in a [1973 coup d'état](https://en.wikipedia.org/wiki/1973_Chilean_coup_d%27%C3%A9tat) led by Augusto Pinochet and supported by the USA.

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
