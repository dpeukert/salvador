#!/bin/bash

# Set shell options
set -e
set -u
set -o 'pipefail'
shopt -s 'dotglob'
shopt -s 'extglob'
shopt -s 'nullglob'

# Check if we have the name parameter and optionally the version string parameter and the URL parameter
if [[ "$#" -lt 2 ]] || [[ "$#" -gt 3 ]]; then
	echo 'ERROR: The package name, a version string and optionally an URL must be provided as the first, second and third arguments, respectively, aborting...'
	exit 1
fi

# Check if the provided parameters are not empty
if [[ -z "$1" ]]; then
	echo 'ERROR: The package name cannot be an empty string, aborting...'
	exit 1
fi

if [[ -z "$2" ]]; then
	echo 'ERROR: The version string cannot be an empty string, aborting...'
	exit 1
fi

if [[ "$#" -ge 3 ]] && [[ -z "$3" ]]; then
	echo 'ERROR: The URL cannot be an empty string, aborting...'
	exit 1
fi

# Check if we have the env vars we need
if [[ -z "${GITLAB_API_TOKEN:-}" ]] || [[ -z "${CI_PROJECT_ID:-}" ]]; then
	echo 'ERROR: Environment variables GITLAB_API_TOKEN and CI_PROJECT_ID are required, aborting...'
	exit 1
fi

# Check if we already have an merge request for this package
mr_query="[$1]"
mr_search_temp_file="$(mktemp)"
mr_search_response_status_code="$(curl --silent --output "${mr_search_temp_file}" --write-out '%{response_code}' --request 'GET' --header 'Content-Type: application/json' --header "Private-Token: ${GITLAB_API_TOKEN}" --get --data-urlencode "source_branch=update/$1" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests?state=opened&order_by=created_at&sort=asc&per_page=100" || true)"
mr_search_response="$(cat "${mr_search_temp_file}")"
rm --force "${mr_search_temp_file}"

if [[ "${mr_search_response_status_code}" -lt 200 ]] || [[ "${mr_search_response_status_code}" -gt 299 ]]; then
	echo 'ERROR: Request to find a unique merge request for this package name failed, aborting...'
	printf 'HTTP status code: %s\n' "${mr_search_response_status_code}"
	printf 'HTTP response body: %s\n' "${mr_search_response}"
	exit 2
fi

mr_search_response="$(echo "${mr_search_response}" | jq --arg mr_query "${mr_query}" -r '.[] | select(.title | contains($mr_query))')"
mr_search_count="$(echo "${mr_search_response}" | jq -rs '. | length')"

if [[ "${mr_search_count}" = '1' ]]; then
	mr_id="$(echo "${mr_search_response}" | jq -rs '.[0].iid // ""')"
elif [[ "${mr_search_count}" = '0' ]]; then
	mr_id=''
else
	echo 'ERROR: Unable to find a unique merge request for this package name, aborting...'
	exit 2
fi

# Check if we have an URL and prepare an merge request title and description based on that
mr_title="${mr_query} Update to $2"

if [[ "$#" -ge 3 ]]; then
	description="Release URL: $3
---
"
else
	description=''
fi

description="${description}Merge request created automatically by [salvador](https://gitlab.com/dpeukert/salvador)."

# Check if we want to set an assignee
assignee=''
if [[ -n "${GITLAB_ASSIGNEE_ID:-}" ]]; then
	assignee="assignee_id=${GITLAB_ASSIGNEE_ID}"
fi

# Create or update the merge request
url="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests"

if [[ -z "${mr_id}" ]]; then
	method='POST'
else
	method='PUT'
	url+="/${mr_id}"
fi

mr_create_update_temp_file="$(mktemp)"
mr_create_update_response_status_code="$(curl --silent --output "${mr_create_update_temp_file}" --write-out '%{response_code}' --request "${method}" --header 'Content-Type: application/json' --header "Private-Token: ${GITLAB_API_TOKEN}" --get --data-urlencode "source_branch=update/$1" --data-urlencode "target_branch=main" --data-urlencode "title=${mr_title}" --data-urlencode "description=${description}" --data-urlencode "${assignee}" --data-urlencode 'remove_source_branch=true' --data-urlencode 'squash=true' "${url}" || true)"
mr_create_update_response="$(cat "${mr_create_update_temp_file}")"
rm --force "${mr_create_update_temp_file}"

if [[ "${mr_create_update_response_status_code}" -lt 200 ]] || [[ "${mr_create_update_response_status_code}" -gt 299 ]]; then
	echo 'ERROR: Request to create/update a merge request for this package name failed, aborting...'
	printf 'HTTP status code: %s\n' "${mr_create_update_response_status_code}"
	printf 'HTTP response body: %s\n' "${mr_create_update_response}"
	exit 2
fi
